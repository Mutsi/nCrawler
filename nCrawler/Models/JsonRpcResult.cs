﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace nCrawler.Models
{
	public class JsonRpcResult<T> where T : class
	{
		[JsonPropertyName("result")]
		public List<T> Result { get; set; }
	}
}
