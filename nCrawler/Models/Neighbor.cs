﻿using System.Text.Json.Serialization;

namespace nCrawler.Models
{
	public class Neighbor
	{
		[JsonPropertyName("addr")]
		public string Addr { get; set; }

		[JsonPropertyName("jsonRpcPort")]
		public int JsonRpcPort { get; set; }
	}
}
