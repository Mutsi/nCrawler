﻿using nCrawler.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace nCrawler.Services
{
	public class LazyCrawler : ICrawler
	{
		private readonly HashSet<string> Open = new HashSet<string>();
		private readonly Regex RegexIp = new Regex("\\/.(.*):");
		private readonly TimeSpan timeout = new TimeSpan(0, 0, 30);
		private HttpClient client;
		private int maxThreadCount = 32;
		private int threadCount;

		private readonly string json = "{\"jsonrpc\":\"2.0\",\"method\":\"getneighbor\",\"params\":{},\"id\":1}";
		private readonly string addressString = "http://{0}:{1}";

		public async Task Start(string startNodeJsonRpcAddr, int maxThreads)
		{
			maxThreadCount = maxThreads;
			threadCount = maxThreadCount;
			client = new HttpClient
			{
				Timeout = timeout
			};

			await GetNeighbours(startNodeJsonRpcAddr);

			ParallelLoopResult parallelResult = Parallel.For(0, Math.Min(Open.Count - 1, threadCount), async (i) =>
			{
				while (true)
				{
					string addr = "";
					lock (Open)
					{
						addr = Open.First();
						Open.Remove(addr);
						Open.TrimExcess();
					}
					await GetNeighbours(addr);
				}
			});

			while (true)
			{
				await Task.Delay(100);
				File.WriteAllText("nodecount.txt", Open.Count.ToString());
			}
		}

		private async Task GetNeighbours(string addr)
		{
			try
			{
				using HttpResponseMessage result = await client.PostAsync(addr, new StringContent(json));
				using Stream stream = await result.Content.ReadAsStreamAsync();
				JsonRpcResult<Neighbor> data = await JsonSerializer.DeserializeAsync<JsonRpcResult<Neighbor>>(stream);

				int newCount = 0;
				data.Result.ForEach(o =>
				{
					string a = string.Format(addressString, RegexIp.Match(o.Addr).Groups[1].Value, o.JsonRpcPort);
					if (!Open.Contains(a))
					{
						Open.Add(a);
						newCount++;
					}
				});
				threadCount = (int)(newCount / (float)data.Result.Count * maxThreadCount) + 1;
			}
			catch
			{
			}
		}
	}
}
