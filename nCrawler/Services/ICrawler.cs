﻿using System.Threading.Tasks;

namespace nCrawler.Services
{
	public interface ICrawler
	{
		Task Start(string startNodeJsonRpcAddr, int maxThreads);
	}
}