﻿using nCrawler.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace nCrawler.Services
{
	public class Crawler : ICrawler
	{
		private readonly ConcurrentBag<string> Open = new ConcurrentBag<string>();
		private readonly ConcurrentDictionary<string, int> Closed = new ConcurrentDictionary<string, int>();
		private readonly Regex RegexIp = new Regex("\\/.(.*):");
		private readonly string json = "{\"jsonrpc\":\"2.0\",\"method\":\"getneighbor\",\"params\":{},\"id\":1}";
		private readonly TimeSpan timeout = new TimeSpan(0, 0, 30);
		private HttpClient client;

		public async Task Start(string startNodeJsonRpcAddr, int maxThreads)
		{
			Console.ReadLine();
			client = new HttpClient
			{
				Timeout = timeout
			};

			await GetNeighbours(startNodeJsonRpcAddr);

			ParallelLoopResult parallelResult = Parallel.For(0, Math.Min(Open.Count - 1, maxThreads), async (i) =>
			  {
				  while (Open.TryTake(out string addr))
				  {
					  await GetNeighbours(addr);
				  }
			  });

			while (Open.Any())
			{
				await Task.Delay(100);
				Console.WriteLine(Open.Count + ":" + Closed.Count);
			}
		}

		private async Task GetNeighbours(string addr)
		{
			try
			{
				using HttpResponseMessage result = await client.PostAsync(addr, new StringContent(json));
				string content = await result.Content.ReadAsStringAsync();
				JsonRpcResult<Neighbor> data = JsonSerializer.Deserialize<JsonRpcResult<Neighbor>>(content);

				IEnumerable<string> addresses = data.Result.Select(o => "http://" + RegexIp.Match(o.Addr).Groups[1].Value + ":" + o.JsonRpcPort);
				foreach (string a in addresses)
				{
					if (!Open.Contains(a) && !Closed.ContainsKey(a))
					{
						Open.Add(a);
					}
				}

				Closed.TryAdd(addr, 1);
			}
			catch
			{
				Closed.TryAdd(addr, 0);
			}
		}
	}
}
