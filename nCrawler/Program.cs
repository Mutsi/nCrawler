﻿using nCrawler.Services;

namespace nCrawler
{
	public class Program
	{
		private static void Main(string[] args)
		{
			ICrawler crawler = new LazyCrawler();
			crawler.Start("http://143.198.123.144:30003", 16).GetAwaiter().GetResult();
		}
	}
}
